# Mac Setup Scripts #

1. Download the repo and extract to a folder on a CLEAN Mac.
2. Open up terminal and navigate to the folder you extracted the files into.
3. Run `chmod +x setup.sh`
4. Run the script `./setup.sh`
5. Enjoy!