#!/bin/bash
# Set some colours!
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"
COL_MAGENTA=$ESC_SEQ"35;01m"
COL_CYAN=$ESC_SEQ"36;01m"

function section_break {
	echo ""
	echo -e "$COL_CYAN========================================================================$COL_RESET"
	echo ""
}

function section_title {
	echo -e "$COL_GREEN==============================================================$COL_RESET"
	echo -e "$COL_GREEN=                                                            =$COL_RESET"
	echo -e "$COL_GREEN=                                                            =$COL_RESET"
	echo -e "$COL_GREEN= $COL_RED $1    				$COL_GREEN=$COL_RESET"
	echo -e "$COL_GREEN=                                                            =$COL_RESET"
	echo -e "$COL_GREEN==============================================================$COL_RESET"
}

clear
# Install Homebrew First!

# First install Xcode command line tools
section_title "Installing Xcode command line tools"
xcode-select --install

section_break

# Then install Homebrew
section_title "Installing Homebrew"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

section_break

# Install the following:
section_title "Install wget, nano, macvim, git, node, terminus, ssh-copy-id, tree and composer"
brew install wget homebrew/dupes/nano macvim grc git node terminus ssh-copy-id tree homebrew/php/composer

section_break

# Other Apps
section_title "Install a load of applications"
brew cask install google-chrome google-drive spotify vlc iterm2 sequel-pro sourcetree cyberduck hipchat firefox virtualbox vagrant imageoptim mysqlworkbench teamviewer

section_break

# Which Editor would you like to install
section_title "Install some editors"

echo "Do you want to install Netbeans?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) brew cask install java netbeans; break;;
		No ) break;;
	esac
done

section_break

echo "Do you want to install Visual Studio Code?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) brew cask install visual-studio-code; break;;
		No ) break;;
	esac
done

section_break

echo "Do you want to install Brackets?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) brew cask install brackets; break;;
		No ) break;;
	esac
done

section_break

echo "Do you want to install Atom?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) brew cask install atom; break;;
		No ) break;;
	esac
done

section_break

echo "Do you want to install Sublime Text 3?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) brew cask install sublime-text; break;;
		No ) break;;
	esac
done

section_break

# Vagrant Plugins
section_title "Installing some Vagrant plugins for you"
vagrant plugin install vagrant-hostsupdater vagrant-share vagrant-triggers

section_break

# Setup folders
section_title "Creating a Sites directory for you in your home folder"
mkdir ~/Sites
section_break

# Setup Git
section_title "Time to setup Git"
git config --global core.editor nano
read -p "Enter your name :" name
git config --global user.name "$name"
read -p "Please enter your email address :" email
git config --global user.email "$email"

section_break

# Setup an SSH Key
section_title "Let's create an SSH key"
ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
section_break

# Mac Nice bits
section_title "Here comes some nice bits for OS X"
defaults write com.apple.dock persistent-others -array-add '{"tile-data" = {"list-type" = 1;}; "tile-type" = "recents-tile";}'; killall Dock
defaults write com.apple.dock mineffect suck; killall Dock

# Setup Bash
section_title "Finally let's get bash setup nicely"
cat bash_profile.txt >> ~/.bash_profile
section_break

cat << "EOF"
__     __         _                  _ _       _                  _ 
 \ \   / /        ( )                | | |     | |                | |
  \ \_/ /__  _   _|/ _ __ ___    __ _| | |   __| | ___  _ __   ___| |
   \   / _ \| | | | | '__/ _ \  / _` | | |  / _` |/ _ \| '_ \ / _ \ |
    | | (_) | |_| | | | |  __/ | (_| | | | | (_| | (_) | | | |  __/_|
    |_|\___/ \__,_| |_|  \___|  \__,_|_|_|  \__,_|\___/|_| |_|\___(_)
                                                                     
EOF